#pragma once

class MyCalculator
{
public:
	static bool Calc(CString Equation, CString &ans, int radix);
	static bool ConvertRad(CString &Num, int radixFrom, int radixTo);
};

